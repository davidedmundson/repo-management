/*
    SPDX-FileCopyrightText: 2013 Test Author <nowhere@noreply.com>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only
*/

#include <iostream>
using namespace std;

int main() {
    cout << "Goodbye, Welt!" << std::endl;
    return 0;
}
