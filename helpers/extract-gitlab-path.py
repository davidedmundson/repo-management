#!/usr/bin/python
import os
import re
import sys
import subprocess

# Check with a Gitaly environment variable first...
if 'GL_PROJECT_PATH' in os.environ:
    print( os.environ['GL_PROJECT_PATH'] )
    sys.exit(0)

# If that fails then try to pull the path from our current working directory
path_match = re.match("^/srv/git/repositories/(.+).git$", os.getcwd())

# Safety Check
# Some versions of Gitaly shipped broken, so as a last ditch attempt to save ourselves, fall back to asking `git config` for our path
if path_match.group(1).startswith('@hashed/'):
    # Ask git config....
    command = ["git", "config", "--local", '--get', 'gitlab.fullpath']
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    print( process.stdout.readline().strip() )
    sys.exit(0)

# Finally if all that failed just print the path we found
print( path_match.group(1) )
sys.exit(0)
